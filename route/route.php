<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

//业务接口请求
//路由定义参考: https://www.kancloud.cn/manual/thinkphp5_1/353962

//路由转发分组匹配转发
Route::group('proxy', 'proxy/proxy_redirect/rewrite');
