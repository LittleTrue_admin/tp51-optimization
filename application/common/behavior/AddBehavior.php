<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

namespace app\common\behavior;

use think\Facade;
use think\Loader;

/**
 * 初始化行为.
 */
class AddBehavior
{
    public function run()
    {
        //注册门面类
        Facade::bind(config('facade.facade'));
        Loader::addClassAlias(config('facade.alias'));
    }
}
