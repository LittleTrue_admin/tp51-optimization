<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */
namespace app\common\validate;

use think\Validate;

/** 基础验证器 拓展类型TP3.2的的数据绑定功能 */
class BasicValidate extends Validate
{
    /** 映射规则:  '(提交或发送过来的)前端字段 =>'(需转化的)后端字段'*/
    protected static $map = [
    ];

    /** 拓展数据绑定机制 */
    public function bindParams(array $data, int $flag = 0)
    {
        $map = self::$map;

        foreach ($data as $key => $value) {
            if (!empty($map[$key])) {
                $ret_data[$map[$key]] = $value;
            } else {
                if (!$flag) {
                    //没有绑定规则的数据也进行字段补充
                    $ret_data[$key] = $value;
                }
            }
        }

        return $ret_data;
    }

}
