<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

namespace app\common\exception;

/**
 * 接口通讯层级错误.
 */
class ApiException extends \Exception
{
    public function __construct(string $message, int $code, \Throwable $previous = null)
    {
        parent::__construct($message, (empty($code) ? 401: $code), $previous);
    }
}
