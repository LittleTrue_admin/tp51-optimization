<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */
namespace app\common\exception;

class ParamsException extends \InvalidArgumentException
{
    public function __construct($message, $code, \Throwable $previous = null)
    {
        parent::__construct($message, (empty($code) ? 401: $code), $previous);
    }
}
