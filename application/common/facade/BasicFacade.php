<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */
namespace app\common\facade;

use think\Facade;

/**
 * @internal
 * @coversNothing
 * 门面为容器中的类提供了一个静态调用接口，相比于传统的静态方法调用， 
 * 带来了更好的可测试性和扩展性，你可以为任何的非静态类库定义一个facade类。
 */
class BasicFacade extends Facade
{

}
