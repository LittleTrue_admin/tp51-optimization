<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

namespace app\index\controller;

use think\Controller;

/**
 * 应用入口控制器.
 */
class Index extends Controller
{
    public function index()
    {
    }
}
