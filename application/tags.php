<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

namespace think;

// 应用行为扩展定义文件
return [
    // 应用初始化
    'app_init' => [],
    // 应用开始
    'app_begin' => [],
    // 模块初始化 -- 进行行为类调用
    'module_init' => ['\app\common\behavior\AddBehavior'],
    // 视图内容过滤
    'view_filter' => [],
    // 日志写入
    'log_write' => [],
    // 应用结束
    'app_end' => [],
    // 控制器操作开始前，进行权限检查
    'action_begin' => [],
];
