<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [huoyuan].
 */
namespace app\proxy\proxyCenter;

/**
 * 转发路由规则配置和参数设置.
 */
class ProxyRuleConfig
{
    /**
     * @var proxyHost 代理HOST
     */
    public static $proxyHost = 'http://hytestapi.kjgoods.com/';

    /**
     * @var cookieHost 代理cookie域
     */
    public static $cookieHost = 'kjgoods.com';

    /**
     * @var sessionKey 代理SESSION操作键值
     */
    public static $sessionKey = 'PHPSESSID';

    /**
     * @var proxyRoute 代理识别路由
     */
    public static $proxyRoute = '/proxy';

    /**
     * @var routeTable 代理路由表
     */
    public static $routeTable = [];
}
