<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [huoyuan].
 */
namespace app\proxy\proxyCenter;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use think\Exception;

/**
 * 转发控制.
 */
class ProxyRequestControl
{
    /**
     * @var setCookieContent 响应set-Cookie内容
     */
    public $setCookieContent = '';

    /**
     * @var responseContent 响应内容
     */
    public $responseContent = '';

    /**
     * @var sendUrl 重定向转发URL
     */
    protected $sendUrl = '';

    /**
     * @var sendMethod 请求方法
     */
    protected $sendMethod = '';

    /**
     * @var cookie 请求Cookie
     */
    protected $cookie = '';

    /**
     * @var cookieHost 请求Cookie作用域
     */
    protected $cookieHost = '';

    /**
     * 初始化请求配置.
     */
    public function __construct($url, $method, $cookie = [], $cookie_host = '')
    {
        if (empty($url) || empty($method)) {
            throw new \Exception('重定向转发参数缺失。');
        }

        if (!empty($cookie) && empty($cookie_host)) {
            throw new \Exception('重定向转发参数cookie错误:未设置hos');
        }

        $this->sendUrl = $url;
        $this->sendMethod = $method;
        $this->cookie = $cookie;
        $this->cookieHost = $cookie_host;
    }

    /**
     * 模拟AJAX网络请求, 自适应触发cookie携带和返回响应cookie, 响应内容。
     *
     * @throws GuzzleException
     * @throws Exception
     *
     * @return bool|string
     */
    public function gateWaySend($data, $options = [])
    {
        //判断请求方法
        if ('GET' == $this->sendMethod) {
            $options['query'] = $data;
        } elseif ('POST' == $this->sendMethod) {
            //默认使用json作为传输数据
            if (is_array($data)) {
                $options['json'] = $data;
            } else {
                $options['form_params'] = $data;
            }
        }

        return $this->request($this->sendMethod, $this->sendUrl, $options, $this->cookie, $this->cookieHost);
    }

    /**
     * Guzzle模拟网络请求
     *
     * @throws GuzzleException
     * @throws Exception
     *
     * @return bool|string
     */
    public function request($method, $url, $options = [], $cookie = [], $cookie_host = '')
    {
        //关闭error异常抛出的问题
        $options['http_errors'] = false;

        //如果没设置超时时间, 默认设置30秒
        if (!isset($options['timeout'])) {
            $options['timeout'] = 50.0;
        }

        //判断请求cookie
        if (!empty($cookie)) {
            $cookieJar = CookieJar::fromArray($cookie, $cookie_host);

            $options['cookies'] = $cookieJar;
        }

        try {
            $httpClient = new Client();
            $response = $httpClient->request($method, $url, $options);

            if (200 !== intval($response->getStatusCode())) {
                throw new \Exception('接口报错:' . $response->getStatusCode());
            }

            //判断是否存在响应的cookie
            $set_cookie = $response->getHeader('Set-Cookie');

            if (!empty($set_cookie)) {
                //进行
                $this->setCookieContent = $set_cookie;
            }

            //判断响应内容
            $response_body = $response->getBody()->getContents();

            if (isJson($response_body)) {
                $this->responseContent = json(json_decode($response_body));
            } else {
                $this->responseContent = $response_body;
            }

            return true;
        } catch (GuzzleException $e) {
            throw $e;
        }
    }
}
