<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

return [
    \app\http\middleware\RequestLog::class,
];
