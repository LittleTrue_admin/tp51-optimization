<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [BWMS].
 *  KKGUAN all rights reserved.
 */
namespace app\master\controller;

use app\common\controller\BasicController;
use app\master\logic\LoginLogic;
use Exception;

/**
 * 系统登录控制器.
 */
class LoginCenter extends BasicController
{
    /**
     * @var LoginLogic
     */
    protected $loginLogic;

    public function __construct(LoginLogic $login_logic)
    {
        parent::__construct();
        $this->loginLogic = $login_logic;
    }

    /**
     * 平台管理员/供货商管理员登录.
     */
    public function adminLogin()
    {
        $param_data = self::$data;
        $this->paramsFilter(
            $param_data,
            [
                'adminName' => 'require',
                'password'     => 'require',
            ],
            [
                'adminName.require' => '账号名必填',
                'password.require'     => '密码必填',
            ]
        );

        extract($param_data);
        
        try{
            //用户登录、获取操作菜单列表
            $admin_info = $this->loginLogic->adminPasswordLogin($adminName, $password);
        } catch (Exception $e) {
            throw $e;
            return $this->jsonError($e->getMessage(),$e->getCode());
        }

        return $this->jsonReturn($admin_info);
    }

    /**
     * 会员用户登录.
     */
    public function userLogin()
    {

        $param_data = self::$data;

        $this->paramsFilter(
            $param_data,
            [
                'userName' => 'require',
                'password'     => 'require',
            ],
            [
                'userName.require' => '账号名必填',
                'password.require'     => '密码必填',
            ]
        );

        extract($param_data);
        
        try{
            //用户登录、获取操作菜单列表
            $user_info = $this->loginLogic->userPasswordLogin($userName, $password);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(),$e->getCode());
        }

        return $this->jsonReturnData($user_info);
    }

    /**
     * 退出登录.
     */
    public function out()
    {
        session(null);
        return $this->jsonSuccess('退出登录成功！');
    }

    /**
     * 检查是否登录.
     */
    public function checkAdminLogin()
    {
        $admin = session(config('session.admin'));

        if (empty($admin)) {
            return json(['status' => false]);
        }

        try{
            //用户登录、获取操作菜单列表
            $admin_info = $this->loginLogic->adminLoginAuth($admin['admin_name'], $admin['password']);

        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(),$e->getCode());
        }

        unset($admin['password']);

        return $this->jsonReturn(['admin' => $admin]);
    }

    /**
     * 检查是否登录.
     */
    public function checkUserLogin()
    {
        $user = session(config('session.user'));

        if (empty($user)) {
            return json(['status' => false]);
        }

        try{
            //用户登录、获取操作菜单列表
            $this->loginLogic->userLoginAuth($user['user_name'], $user['password']);
        } catch (Exception $e) {
            return $this->jsonError($e->getMessage(),$e->getCode());
        }

        unset($user['password']);

        return $this->jsonReturn(['user' => $user]);
    }
}

