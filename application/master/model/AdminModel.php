<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [BWMS].
 *  DZ all rights reserved.
 */

namespace app\master\model;

use app\common\model\BasicModel;
use think\Db;

/**
 * 系统管理员模型.
 *
 * Class SystemUserModel
 */
class AdminModel extends BasicModel
{
    /**
     * SystemUserModel constructor.
     */
    public function __construct( $data = [])
    {
        $this->pk = 'id';
        $this->table = 'res_admin';
        parent::__construct($data);
    }

        //供货商模型绑定
        public function supplier()
        {
            return $this->belongsTo('SupplierModel', 'supplier', 'supplier_code')->bind(['supplier_name', 'supplier_id']);
        }

        
    /**
     * 通过key, value查询管理员信息.
     *
     * @param $key
     * @param $value
     * @param  string  $field
     * @throws \think\Exception
     * @return null|array|\PDOStatement|string|\think\Model
     */
    public function adminInfo($key, $value, $field = '*')
    {
        return $this->getItem($field, [$key => $value]);
    }

    /**
     * 查询单条系统管理员数据.
     *
     * @throws \Exception
     */
    public function getAdminItem( $where = [],  $field = '*')
    {

        try {
            return $this->getItem($field, $where);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * 查询单条系统管理员和供货商数据.
     *
     * @throws \Exception
     */
    public function getAdminSupplierItem( $where = [],  $field = '*')
    {
        try {
            return self::with(['supplier'])
                ->field($field)
                ->where($where)
                ->find()->toArray();

        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * 通过用户名获取用户信息.
     *
     * @param string $userName 用户名
     *
     * @throws \Exception
     */
    public function getItemByAdminName( $admin_name)
    {
        if (empty($admin_name)) {
            errorException(1000001);
        }

        return $this->getAdminItem(['admin_name' => $admin_name]);
    }


    /**
     * 通过id获取管理员信息.
     *
     * @param string $adminId 用户id
     *
     * @throws \Exception
     */
    public function getItemByAdminId( $admin_id)
    {
        //TODO
    }

    /**
     * 新增管理员.
     *
     * @param $userName
     * @param $password
     * @param $authorize
     * @param $createBy
     * @throws \think\Exception
     * @return int|string
     */
    public function insertAdmin($admin_name, $password, $authorize, $createBy)
    {
        //TODO
    }

    /**
     * 验证输入密码
     */
    public function checkPassword($password, $where = [])
    {
        if (empty($password)) {
            errorException(2000001);
        }

        if (empty($where)) {
            errorException(2000001);
        }

        try {
            $item = $this->getItem('password', $where);
            if (md5($password) != $item['password']) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
