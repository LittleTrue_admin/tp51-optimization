<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [BWMS].
 *  DZ all rights reserved.
 */

namespace app\master\model;

use app\common\model\BasicModel;
use think\Db;

/**
 * 用户会员模型.
 *
 * Class SystemUserModel
 */
class UserModel extends BasicModel
{
    /**
     * SystemUserModel constructor.
     */
    public function __construct( $data = [])
    {
        $this->pk = 'id';
        $this->table = 'res_user';
        parent::__construct($data);
    }

    /**
     * 通过key, value查询会员用户信息.
     *
     * @param $key
     * @param $value
     * @param  string  $field
     * @throws \think\Exception
     * @return null|array|\PDOStatement|string|\think\Model
     */
    public function userInfo($key, $value, $field = '*')
    {
        return $this->getItem($field, [$key => $value]);
    }

    /**
     * 查询单条系统会员用户数据.
     *
     * @throws \Exception
     */
    public function getUserItem( $where = [],  $field = '*')
    {
        try {
            return $this->getItem($field, $where);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * 通过用户名获取用户信息.
     *
     * @param string $userName 用户名
     *
     * @throws \Exception
     */
    public function getItemByUserName( $user_name)
    {
        if (empty($user_name)) {
            errorException(1000001);
        }

        return $this->getUserItem(['user_name' => $user_name, 'is_deleted' => 0]);
    }


    /**
     * 通过id获取会员用户信息.
     *
     * @param string $userId 用户id
     *
     * @throws \Exception
     */
    public function getItemByUserId( $user_id)
    {
      //TODO
    }

    /**
     * 新增会员用户.
     *
     * @param $user_name
     * @param $password
     * @param $authorize
     * @param $createBy
     * @throws \think\Exception
     * @return int|string
     */
    public function insertUser($user_name, $password, $authorize, $createBy)
    {
        //TODO
    }

    /**
     * 验证输入密码
     */
    public function checkPassword($password, $where = [])
    {
        if (empty($password)) {
            errorException(2000001);
        }

        if (empty($where)) {
            errorException(2000001);
        }

        try {
            $item = $this->getItem('password', $where);
            if (md5($password) != $item['password']) {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
