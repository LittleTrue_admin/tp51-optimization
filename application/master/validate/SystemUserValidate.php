<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [BWMS].
 *  KKGUAN all rights reserved.
 */
namespace app\master\validate;

use think\Validate;

class SystemUserValidate extends Validate
{
    protected $rule = [
        'account|用户名'        => 'require|max:50|unique:system_user',
        'phone|手机号'          => 'require|mobile',
        'email|电子邮箱'        => 'require|email',
        'id_card|身份证'        => 'require|idCard',
        'real_name|真实姓名'    => 'require|max:45',
        // 'session_id'    => 'require',
        'authorize|权限'        => 'require',
        'id'                    => 'require',
        'password|密码'         => 'require',
        'repassword|新密码'     => 'require|confirm:password',
    ];

    protected $message = [
        'repassword.confirm' => '两次密码不一致',
    ];

    protected $scene = [
        'user'      => ['account','phone','email','id_card','real_name','authorize'],
        'password'  => ['id','password','repassword'],
    ];

}

