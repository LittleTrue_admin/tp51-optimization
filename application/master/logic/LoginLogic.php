<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [BWMS].
 *  KKGUAN all rights reserved.
 */

namespace app\master\logic;

use app\master\model\AdminModel;
use app\master\model\UserModel;
use app\master\model\SupplierModel;
/**
 * 登录逻辑层
 */
class LoginLogic
{
    /**
     * @var UserModel
     */
    protected $userM;

    /**
     * @var AdminModel
     */
    protected $adminM;

    /**
     * @var SupplierModel
     */
    protected $supplierM;

    public function __construct(UserModel $user_model,  AdminModel $admin_model, SupplierModel $supplier_model)
    {
        
        $this->userM = $user_model;
        $this->adminM = $admin_model;
        $this->supplierM = $supplier_model;
    }

    /**
     * 管理员账号密码登录.
     *
     * @param string $admin_name 用户名
     * @param string $password 密码
     *
     * @throws Exception
     */
    public function adminPasswordLogin($admin_name,  $password)
    {
        try {
            if (strlen($password) < 4) {
                errorException(1001951);
            }

            /* 管理员信息验证 */
            $admin = $this->adminM->getItemByAdminName($admin_name);


            if (empty($admin)) {
                errorException(1001952);
            }

            if (md5($password) !== $admin['password']) {
                errorException(1001953);
            }

            if ($admin['is_lock']) {
                errorException(1001954);
            }

            $admin_info = $this->adminM->getAdminSupplierItem(['id' =>  $admin['id']] );

            //寻找关联供应链
            $addition = $this->supplierM->getSupplierInfo($admin_info['supplier']);

            if (empty($addition) && !$admin_info['is_super_admin']) {
                errorException(1001962);
            }
    
            //供应链名
            $company_name = $addition['supplier_name'];

            //默认是供应链角色
            $is_pavilion_admin = 1; 

            if (!empty($admin_info['facturer_code'])) {
                //供应链
                $is_pavilion_admin = 0;
                //子供货方名称
                $company_name = $admin_info['real_name'];
            } 
        
            //超管和供应链一起时,屏蔽供应链身份
            1 == $admin_info['is_super_admin'] ? $is_pavilion_admin = 0 : '';

            $admin_info['is_pavilion_admin'] = $is_pavilion_admin;
            $admin_info['companyname'] = $company_name;

            $token = createToken(time() . $admin_info['admin_name']);

            cookie('token_admin', $token);
            cookie('admin_name', $admin_info['admin_name']);
            cookie('is_super_admin', $admin_info['is_super_admin']);
            cookie('companyname', $company_name);
            cookie('is_pavilion_admin', $is_pavilion_admin);

            session(config('session.admin'), $admin_info);

            unset($admin_info['id'], $admin_info['password']);
            return ['admin' => $admin_info];
        } catch (\Exception $e) {
            throw $e;
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 管理员验证登录.
     *
     * @param string $user_name 用户名
     * @param string $password 密码
     *
     * @throws Exception
     */
    public function adminLoginAuth($admin_name,  $password)
    {
        try {
            if (strlen($password) < 4) {
                errorException(1001951);
            }

            $admin = $this->adminM->getItemByAdminName($admin_name);

            if (empty($admin)) {
                errorException(1001952);
            }

            if ($password !== $admin['password']) {
                errorException(1001953);
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 会员用户账号密码登录.
     *
     * @param string $user_name 用户名
     * @param string $password 密码
     *
     * @throws Exception
     */
    public function userPasswordLogin( $user_name,  $password)
    {
        try {
            if (strlen($password) < 4) {
                errorException(1001951);
            }

            /* 用户信息验证 */
            $user = $this->userM->getItemByUserName($user_name);

            if (empty($user)) {
                errorException(1001952);
            }

            if (md5($password) !== $user['password']) {
                errorException(1001953);
            }

            if (empty($user['user_status'])) {
                errorException(1001954);
            }

             //检查账号状态
            switch ($user['user_status']) {
                case 0:
                    errorException(1001961);
                    break;
                case 1:
                    //保存登录信息
                    $avatar = !empty($user['avatar']) ? config('path.SHOW_PATH') . $user['avatar'] : config('path.DEFAULT_IMG_PATH');
                    
                    //待定
                    $token = createToken(time() . $user['user_name']);

                    cookie('username', $user['user_name']);
                    cookie('companyname', $user['company_name']);
                    cookie('avatar', $avatar);
                    cookie('token', $token);

                    $user['avatar'] = !empty($user['avatar']) ? config('path.SHOW_PATH') . $user['avatar'] : config('path.DEFAULT_IMG_PATH');
                    session(config('session.user'), $user);
                    return true;
                    break;
                case 2:
                    errorException(1001959);
                    break;
                case 3:
                    errorException(1001954);
                    break;
                default:
                    errorException(1001960);
                    break;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * 会员用户验证登录.
     *
     * @param string $user_name 用户名
     * @param string $password 密码
     *
     * @throws Exception
     */
    public function userLoginAuth($user_name,  $password)
    {
        try {
            if (strlen($password) < 4) {
                errorException(1001951);
            }

            /* 用户信息验证 */
            $user = $this->userM->getItemByUserName($user_name);

            if (empty($user)) {
                errorException(1001952);
            }

            if ($password !== $user['password']) {
                errorException(1001953);
            }

            if (empty($user['user_status'])) {
                errorException(1001954);
            }

             //检查账号状态
            switch ($user['user_status']) {
                case 0:
                    errorException(1001961);
                    break;
                case 2:
                    errorException(1001959);
                    break;
                case 3:
                    errorException(1001954);
                    break;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }
}
