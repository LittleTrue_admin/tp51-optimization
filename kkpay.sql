-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2020-09-07 12:27:04
-- 服务器版本： 5.7.29
-- PHP 版本： 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `kkpay`
--

-- --------------------------------------------------------

--
-- 表的结构 `log_system_receipt`
--

CREATE TABLE `log_system_receipt` (
  `receipt_id` int(11) UNSIGNED NOT NULL,
  `receipt_content` text NOT NULL COMMENT '回执内容',
  `receipt_business` varchar(45) NOT NULL COMMENT '回执主体业务',
  `receipt_time` datetime NOT NULL COMMENT '回执接受时间',
  `business_source` varchar(60) DEFAULT '' COMMENT '业务主体数据标志号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='回执回应记录表';

-- --------------------------------------------------------

--
-- 表的结构 `log_system_receive`
--

CREATE TABLE `log_system_receive` (
  `receive_id` int(11) UNSIGNED NOT NULL,
  `receive_content` text NOT NULL COMMENT '接收内容',
  `receive_business` varchar(45) NOT NULL COMMENT '接收主体业务',
  `receive_time` datetime NOT NULL COMMENT '接收接受时间',
  `business_source` varchar(60) DEFAULT '' COMMENT '接收业务主体数据标志号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='回执回应记录表';

-- --------------------------------------------------------

--
-- 表的结构 `log_system_request`
--

CREATE TABLE `log_system_request` (
  `request_id` int(11) NOT NULL,
  `request_context` text NOT NULL COMMENT '请求内容',
  `request_business` varchar(45) NOT NULL COMMENT '请求业务主体',
  `request_time` datetime NOT NULL COMMENT '请求时间',
  `business_source` varchar(60) DEFAULT '' COMMENT '业务主体数据标志号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统发起请求记录表(除订单报文生成)';

-- --------------------------------------------------------

--
-- 表的结构 `system_auth`
--

CREATE TABLE `system_auth` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(20) NOT NULL COMMENT '权限名称',
  `status` tinyint(1) UNSIGNED DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `sort` smallint(6) UNSIGNED DEFAULT '0' COMMENT '排序权重',
  `desc` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `create_by` bigint(11) UNSIGNED DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '最近操作更新时间',
  `last_operator` int(11) NOT NULL DEFAULT '0' COMMENT '最近操作人id',
  `menu_access` varchar(255) NOT NULL DEFAULT '' COMMENT '允许访问显示的菜单模块列表'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统权限表';

--
-- 转存表中的数据 `system_auth`
--

INSERT INTO `system_auth` (`id`, `title`, `status`, `sort`, `desc`, `create_by`, `create_at`, `update_time`, `last_operator`, `menu_access`) VALUES
(1, '默认角色', 1, 1, '初始化用户时，使用的默认角色', 1, '2018-07-17 19:16:49', 1501301325, 1, '[\"recordEntity\",\"goodsShelf\",\"userList\",\"moveWarehouse\",\"goodsRecord\",\"stockArea\",\"stockSite\",\"enterWarehouse\",\"outWarehouse\",\"goodsStock\",\"operationLog\",\"operationPDA\"]'),
(2, '系统管理员', 1, 2, '系统管理员权限，包括全部权限', 1, '2018-07-03 23:14:16', 1501301325, 1, '[\"recordEntity\",\"goodsShelf\",\"userList\",\"moveWarehouse\",\"goodsRecord\",\"stockArea\",\"stockSite\",\"enterWarehouse\",\"outWarehouse\",\"goodsStock\",\"operationLog\"]');

-- --------------------------------------------------------

--
-- 表的结构 `system_auth_node`
--

CREATE TABLE `system_auth_node` (
  `id` int(11) UNSIGNED NOT NULL,
  `auth` bigint(20) UNSIGNED DEFAULT NULL COMMENT '角色ID',
  `node` varchar(200) DEFAULT NULL COMMENT '节点路径'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与权限节点关系表';

--
-- 转存表中的数据 `system_auth_node`
--

INSERT INTO `system_auth_node` (`id`, `auth`, `node`) VALUES
(1, 1, 'master/user/userList');

-- --------------------------------------------------------

--
-- 表的结构 `system_config`
--

CREATE TABLE `system_config` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '配置编码',
  `value` varchar(500) NOT NULL DEFAULT '' COMMENT '配置值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数配置';

--
-- 转存表中的数据 `system_config`
--

INSERT INTO `system_config` (`id`, `name`, `value`) VALUES
(1, 'app_name', '测试环境'),
(2, 'site_name', '测试环境'),
(3, 'app_version', '1.0'),
(4, 'site_copy', '©版权所有 2014-2020 广州市徳智科技发展有限公司'),
(7, 'miitbeian', '粤ICP备15060151号'),
(8, 'wave_config', '{\"wave_order_number\":\"16\",\"describe\":\"波次单订单数\"}');

-- --------------------------------------------------------

--
-- 表的结构 `system_node`
--

CREATE TABLE `system_node` (
  `id` int(11) UNSIGNED NOT NULL,
  `node` varchar(100) DEFAULT NULL COMMENT '节点代码(细分路径的url) 或者 菜单的整体模块代码',
  `title` varchar(500) DEFAULT NULL COMMENT '节点标题',
  `is_auth` tinyint(1) UNSIGNED DEFAULT '1' COMMENT '是否启动RBAC权限控制',
  `is_login` tinyint(1) UNSIGNED DEFAULT '1' COMMENT '是否启动登录状态控制',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='需要进行权限控制(登录/角色权限)的系统节点表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `system_node`
--

INSERT INTO `system_node` (`id`, `node`, `title`, `is_auth`, `is_login`, `create_at`) VALUES
(1, 'master/user/userList', '用户列表管理', 1, 1, '2017-08-23 07:45:44'),
(2, 'master/account/index', '账号信息获取', 0, 1, '2017-08-23 07:45:44'),
(3, 'master/account/saveAccount', '保存修改账号信息', 0, 1, '2020-07-03 08:32:29'),
(4, 'master/account/savePassword', '保存修改的登录密码', 0, 1, '2020-07-03 08:33:12');

-- --------------------------------------------------------

--
-- 表的结构 `system_user`
--

CREATE TABLE `system_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `account` varchar(50) NOT NULL COMMENT '账户名',
  `password` varchar(100) NOT NULL COMMENT '账户密码',
  `phone` varchar(16) NOT NULL DEFAULT '' COMMENT '联系手机号',
  `email` varchar(65) DEFAULT NULL COMMENT '邮箱',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证',
  `real_name` varchar(45) DEFAULT NULL COMMENT '真实姓名',
  `login_time` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用 2:注销)',
  `is_deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否软删除: 0 否 1 是',
  `session_id` varchar(50) NOT NULL DEFAULT '' COMMENT '用户session_id',
  `authorize` tinyint(3) NOT NULL DEFAULT '1' COMMENT '权限ID集合',
  `is_super` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否超级管理员 0否 1是',
  `warehouse_id` int(11) NOT NULL DEFAULT '1' COMMENT '关联的仓库id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统管理员表';

--
-- 转存表中的数据 `system_user`
--

INSERT INTO `system_user` (`id`, `account`, `password`, `phone`, `email`, `id_card`, `real_name`, `login_time`, `create_time`, `status`, `is_deleted`, `session_id`, `authorize`, `is_super`, `warehouse_id`) VALUES
(1, 'super', 'c56d0e9a7ccec67b4ea131655038d604', '13022076770', '', '', '超级管理员', 1595060103, '2020-06-07 22:20:01', 1, 0, 'qdp5rbltmgsmh3bm0ndcjull1b', 1, 1, 1);

--
-- 转储表的索引
--

--
-- 表的索引 `log_system_receipt`
--
ALTER TABLE `log_system_receipt`
  ADD PRIMARY KEY (`receipt_id`);

--
-- 表的索引 `log_system_receive`
--
ALTER TABLE `log_system_receive`
  ADD PRIMARY KEY (`receive_id`);

--
-- 表的索引 `log_system_request`
--
ALTER TABLE `log_system_request`
  ADD PRIMARY KEY (`request_id`);

--
-- 表的索引 `system_auth`
--
ALTER TABLE `system_auth`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_system_auth_title` (`title`) USING BTREE,
  ADD KEY `index_system_auth_status` (`status`) USING BTREE;

--
-- 表的索引 `system_auth_node`
--
ALTER TABLE `system_auth_node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_auth_auth` (`auth`) USING BTREE,
  ADD KEY `index_system_auth_node` (`node`) USING BTREE;

--
-- 表的索引 `system_config`
--
ALTER TABLE `system_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE_NAME` (`name`);

--
-- 表的索引 `system_node`
--
ALTER TABLE `system_node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX_ISAUTH` (`is_auth`),
  ADD KEY `INDEX_SYSTEMNODE` (`node`);

--
-- 表的索引 `system_user`
--
ALTER TABLE `system_user`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `log_system_receipt`
--
ALTER TABLE `log_system_receipt`
  MODIFY `receipt_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `log_system_receive`
--
ALTER TABLE `log_system_receive`
  MODIFY `receive_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `log_system_request`
--
ALTER TABLE `log_system_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `system_auth`
--
ALTER TABLE `system_auth`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `system_auth_node`
--
ALTER TABLE `system_auth_node`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `system_config`
--
ALTER TABLE `system_config`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用表AUTO_INCREMENT `system_node`
--
ALTER TABLE `system_node`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `system_user`
--
ALTER TABLE `system_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
