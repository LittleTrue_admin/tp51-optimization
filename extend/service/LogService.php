<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  DZ all rights reserved.
 */

namespace service;

use think\Db;

/**
 * 日志服务
 */
class LogService
{
    /**
     * [系统日志]收到数据日志记录输出.
     *
     * @param string $action       行为/操作产生的方法
     * @param string $receive_data 错误日志内容
     */
    public static function writeReceiveLog($action, $receive_data, $business_source = '')
    {
        if (empty($receive_data) || empty($action)) {
            return false;
        }

        $time = date('Y-m-d H:i:s', time());

        Db::name('log_system_receive')->insert(
            [
                'receive_content'  => $receive_data,
                'receive_business' => $action,
                'receive_time'     => $time,
                'business_source'  => $business_source,
            ]
        );

        return true;
    }

    /**
     * [数据交互]写入数据交互记录[request内容].
     *
     * @param string $action   行为/操作产生的方法  system
     * @param string $log_data 日志内容
     */
    public static function writeRequestLog($action, $log_data, $business_source = '')
    {
        if (empty($log_data) || empty($action)) {
            return false;
        }

        $time = date('Y-m-d H:i:s', time());

        Db::name('log_system_request')->insert(
            [
                'request_context'  => $log_data,
                'request_business' => $action,
                'request_time'     => $time,
                'business_source'  => $business_source,
            ]
        );

        return true;
    }

    /**
     * [系统日志]错误日志记录输出.
     *
     * @param string $action     行为/操作产生的方法
     * @param string $error_data 错误日志内容
     * @param string $operator   操作主体
     */
    public static function errorLog($action, $error_data, $operator = '')
    {
        if (empty($error_data) || empty($action)) {
            return false;
        }

        $time = date('Y-m-d H:i:s', time());

        Db::name('log_system_error')->insert(
            [
                'content'  => $error_data,
                'business' => $action,
                'log_time' => $time,
                'operator' => $operator,
            ]
        );

        return true;
    }

    /**
     * [数据交互]接收数据交互记录[request内容].
     *
     * @param string $action   行为/操作产生的方法  system
     * @param string $log_data 日志内容
     */
    public static function writeReceiptLog($action, $log_data, $business_source = '')
    {
        if (empty($log_data) || empty($action)) {
            return false;
        }
        $time = date('Y-m-d H:i:s', time());

        Db::name('log_system_receipt')->insert(
            [
                'receipt_content'  => $log_data,
                'receipt_business' => $action,
                'receipt_time'     => $time,
                'business_source'  => $business_source,
            ]
        );

        return true;
    }
}
