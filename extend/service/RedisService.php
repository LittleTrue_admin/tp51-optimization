<?php
/**
 * This file is part of Terp.
 *
 * @link     http://terp.example.com
 * @license  http://192.168.30.119:10080/KKERP/erp
 */

namespace service;

/**
 * Redis相关操作.
 */
class RedisService extends \Redis
{
    /**
     * @var \Redis
     */
    protected $redisClient;

    public function __construct(\Redis $redisClient)
    {
        parent::__construct();
        $config = config('redis.');
        $this->redisClient = $redisClient;
        $this->redisClient->connect($config['host'], $config['port'], $config['time_out']);
        $this->redisClient->auth($config['password']);
        $this->redisClient->select($config['select']);
    }

    /**
     * 删除某个redis.
     *
     * @param string $key Redis的key
     */
    public function deleteRedis(string $key): int
    {
        $result = 0;

        if (!empty($key)) {
            $result = $this->redisClient->del($key);
        }

        return $result;
    }

    /**
     * 获取redis类.
     */
    public function getRedis(): \Redis
    {
        return $this->redisClient;
    }
}
