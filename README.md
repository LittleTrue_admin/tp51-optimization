## 修改日志
基于发展的眼光, 以及切合团队开发速度的适度设计原则, 使用以下层次以及相应功能:
    1、Model 将数据库操作按照功能操作的粒度切分在Model中, 由Model统一处理这些功能数据库操作, 其他层无法直接操作 -- (控制数据的存储获取)。
    2、Controller 部分业务可以直接在Controller中调用Model完成其业务操作, 但是业务逻辑复杂且大概率复用的部分,额外封装到Logic中 -- (控制业务逻辑的调取, 部分控制业务数据的处理)
    3、Logic由C层调用, 负责做些处理并调用Model层完成数据的入库等和数据库打交道的事情 -- (控制业务数据的处理)。
    4、基于IOC + COMPOSER 封装 Service层 -- (外部应用业务对接处理)。

### 基于TP5.1.*版本进行二次封装:
    * 改变部分底层源码(待更新)
    * 删除通用代码审核机制不支持的部分框架代码和文件(待更新)
    * 根据业务分层, 对框架结构进一步细分, 细分层次, 处理业务文件目录
    * 集成公共基础类
    * 集成系统开发底层支持服务service支持类
    * 中间层封装前后端下CORS处理封装
    * 基于swoole的websocket支持处理
    * docker部署预支持
    * 代码格式化规则文件设置
    * 初步实现权限机制

.env-example  复制一份修改为.env, 配置本地参数。


************************************
## ThinkPHP5.1对底层架构做了进一步的改进，减少依赖，其主要特性包括：
 + 采用容器统一管理对象
 + 支持Facade
 + 注解路由支持
 + 路由跨域请求支持
 + 配置和路由目录独立
 + `取消系统常量`
 + `取消Loadder支持`
 + 助手函数增强
 + 类库别名机制
 + 增加条件查询
 + 改进查询机制
 + 配置采用二级
 + 依赖注入完善
 + 支持`PSR-3`日志规范
 + 中间件支持（V5.1.6+）
 + Swoole/Workerman支持（V5.1.18+）

## TP5 在线手册

+ [完全开发手册](https://www.kancloud.cn/manual/thinkphp5_1/content)
+ [升级指导](https://www.kancloud.cn/manual/thinkphp5_1/354155) 

### 目录和文件

*   目录不强制规范，驼峰和小写+下划线模式均支持；
*   类库、函数文件统一以`.php`为后缀；
*   类的文件名均以命名空间定义，并且命名空间的路径和类库文件所在路径一致；
*   类名和类文件名保持一致，统一采用驼峰法命名（首字母大写）；

### 编码规范
*  参考: https://www.yuque.com/vro2bg/vhdf6m/vyd6q2