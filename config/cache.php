<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

// | 缓存设置
// +----------------------------------------------------------------------

return [
    // 选择模式
    'type'    => 'complex',
    //默认缓存为redis
    'default' => [
        'type'     => 'Redis',
        'prefix'   => env('CACHE_REDIS_PREFIX'),
        'host'     => env('CACHE_REDIS_HOST'),
        'port'     => env('CACHE_REDIS_PORT'),
        'password' => env('CACHE_REDIS_AUTH'),
        'timeout'  => 300,
    ],
    //可选文件缓存
    'file' => [
        'type'   => 'File',
        'path'   => env('CACHE_FILE_PATH'),
        'prefix' => env('CACHE_FILE_PREFIX'),
        'expire' => env('CACHE_FILE_EXPIRE'),

        
    ],
];
