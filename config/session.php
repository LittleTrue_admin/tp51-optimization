<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

// | 会话设置
// +----------------------------------------------------------------------

return [
    // SESSION 前缀
    'prefix' => env('SESSION_PREFIX'),
    // 驱动方式 支持redis memcache memcached
    'type' => env('SESSION_TYPE', 'redis'),
    //过期时间
    'expire' => 0,
    // 是否自动开启 SESSION
    'auto_start' => true,
    //redis配置
    'host'     => env('SESSION_HOST'),
    'port'     => env('SESSION_PORT', 6379),
    'time_out' => 1,
    'select'   => env('SESSION_DATABASE', 0),
    'password' => env('SESSION_AUTH'),
    'user'     => env('USER_SESSION'),
    'admin'     => env('ADMIN_SESSION'),
];
