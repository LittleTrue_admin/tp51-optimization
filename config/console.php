<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

// | 控制台配置
// +----------------------------------------------------------------------
return [
    'name'      => 'Think Console',
    'version'   => '0.1',
    'user'      => null,
    'auto_path' => env('app_path') . 'command' . DIRECTORY_SEPARATOR,
];
