<?php
    /**
     * 静态代理
     */
    
    return [
        'facade'=>[
            // 静态代理类                                 被代理类
            'app\common\facade\BasicFacade'=>'app\common\validate\BasicValidate',
        ],
        'alias'=>[
            // 别名设置
            'BasicFacade'=>'app\common\facade\BasicFacade',
        ]
    ];
