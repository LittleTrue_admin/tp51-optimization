<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */
return [
    //后端访问地址设置--与部分下载任务url组建相关
    'api_path' => env('API_URL'),

    //模板文件存储位置
    'template_path' => env('ROOT_PATH') . 'private/template/',

    //json文件数据库存放位置（计量单位unit_json，国家代码country_json，币值单位currency_json, 区域代码district_json）
    'json_path' => env('ROOT_PATH') . 'private/json/',

    //系统公私钥存放路径
    'key_path' => env('ROOT_PATH') . 'private/key/',

     //上传文件位置
     'upload_path' => env('ROOT_PATH') . 'public/uploadFile/',

     //Excel模板文件位置
     'excel_path' => env('ROOT_PATH') . 'public/demo/',

];
