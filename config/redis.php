<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

// | 缓存redis设置 -- 非框架集成调用方式的配置
// +----------------------------------------------------------------------

return [
    'prefix'   => env('CACHE_REDIS_PREFIX'),
    'host'     => env('CACHE_REDIS_HOST'),
    'port'     => env('CACHE_REDIS_PORT'),
    'time_out' => 1,
    'select'   => env('CACHE_DATABASE', 1),
    'password' => env('CACHE_REDIS_AUTH'),
];
