<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */
return [
    //默认OSS存储方式  local qiniu  aliyun
    'storage_type_default' => 'qiniu',
    //阿里云OSS -- 已配置待集成入项目
    'aliyun' => [
        'bucket'     => env('ALI_OSS_bucket', 'the-colorist-test'),
        'domain'     => env('ALI_OSS_DOMAIN', 'cdn-tc-private.kjgoods.com'),
        'access_key' => env('ALI_OSS_ACCESS_KEY'),
        'secret_key' => env('ALI_OSS_SECRET_KEY'),
        'region'     => env('ALI_OSS_REGION', '??'),
        'is_https'   => env('ALI_OSS_IS_HTTPS', 'http'),
    ],
    //七牛云OSS
    'qiniu' => [
        'bucket'     => env('QINIU_OSS_BUCKET', 'the-colorist'),
        'domain'     => env('QINIU_OSS_DOMAIN', 'cdn-tc.kjgoods.com'),
        'access_key' => env('QINIU_OSS_ACCESS_KEY', 'N6xNlVdccebDTaHgKMmcG14kWewg3TBMcb9Dazqn'),
        'secret_key' => env('QINIU_OSS_SECRET_KEY', 'b00exsyJfoSTU7BTsLWMukf7Vwm0P_1xfQPtoRB2'),
        'region'     => env('QINIU_OSS_REGION', '华南'),
        'is_https'   => env('QINIU_OSS_IS_HTTPS', 'http'),
    ],
];
