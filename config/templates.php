<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [BWMS].
 *  DZ all rights reserved.
 */

return [
    //设置excel字段读取的 模板 与 列数 的字段转化

    //订单出库导入
    'ORDER_IMPORT' => [
        'no'                    => 0,
        'client_code'           => 1,
        'warehouse_code'        => 2,
        'order_no'              => 3,
        'user_order_no'         => 4,
        'pay_no'                => 5,
        'create_time'           => 6,
        'order_type'            => 7,
        'order_doc_name'        => 8,
        'order_doc_tel'         => 9,
        'order_doc_id'          => 10,
        'order_doc_type'        => 11,
        'currency'              => 12,
        'other_payment'         => 13,
        'actual_amount_paid'    => 14,
        'other_charges'         => 15,
        'insurance_fee'         => 16,
        'shipping_fee'          => 17,
        'tax'                   => 18,
        'goods_fee'             => 19,
        'departure_country'     => 20,
        'consignee'             => 21,
        'consignee_tel'         => 22,
        'province'              => 23,
        'city'                  => 24,
        'district'              => 25,
        'address'               => 26,
        'consignor'             => 27,
        'consignor_tel'         => 28,
        'consignor_address'     => 29,
        'package_number'        => 30,
        'pay_amount'            => 31,
        'payer_name'            => 32,
        'payer_document_type'   => 33,
        'payer_document_number' => 34,
        'payer_phone_number'    => 35,
        'pay_inital_request'    => 36,
        'pay_inital_response'   => 37,
        'pay_time'              => 38,
        'note'                  => 39,
    ],

    //订单字段验证
    'ORDER_IMPORT_VALIDATE' => [
        'no|序号'                 => 'require',
        'client_code|客户编号'      => 'require',
        'warehouse_code|出库仓库编号' => 'require',
        // 'order_no'           => '',
        'user_order_no|客户订单自编编号'        => 'require',
        'pay_no|支付单号'                   => 'require',
        'order_type|出库性质'               => 'require|in:1210,9610,0110,0000',
        'order_doc_name|订购人姓名'          => 'require',
        'order_doc_tel|订购人电话'           => 'require',
        'order_doc_id|订购人证件号码'          => 'require',
        'order_doc_type|订购人证件类型'        => 'require|in:0,1',
        'currency|币制'                   => 'require',
        'other_payment|抵付金额'            => 'require',
        'actual_amount_paid|实际支付金额'     => 'require',
        'other_charges|其他费用'            => 'require',
        'insurance_fee|保费'              => 'require',
        'shipping_fee|运费'               => 'require',
        'tax|税费'                        => 'require',
        'goods_fee|商品总金额'               => 'require',
        'departure_country|启运国'         => 'require',
        'consignee|收货人姓名'               => 'require',
        'consignee_tel|收货人电话'           => 'require',
        'province|收货省'                  => 'require',
        'city|收货市'                      => 'require',
        'district|收货区'                  => 'require',
        'address|收货人地址'                 => 'require',
        'consignor|发货人姓名'               => 'require',
        'consignor_tel|发货人电话'           => 'require',
        'consignor_address|发货人地址'       => 'require',
        'package_number|包装件数'           => 'require',
        'pay_amount|支付金额'               => 'require',
        'payer_name|支付人姓名'              => 'require',
        'payer_document_type|支付人证件类型'   => 'require',
        'payer_document_number|支付人证件号码' => 'require',
        'payer_phone_number|支付人手机号'     => 'require',
        'pay_inital_request|支付原始请求信息数据' => 'require',
        'pay_inital_response|支付原始响应信息'  => 'require',
        'pay_time|支付时间'                 => 'require',
        // 'note'               => '',
    ],

    //订单商品导入
    'ORDER_GOODS_IMPORT' => [
        'no'            => 0,
        'client_code'   => 1,
        'user_order_no' => 2,
        'goods_name'    => 3,
        'bar_code'      => 4,
        'specification' => 5,
        'goods_price'   => 6,
        'goods_number'  => 7,
        'goods_total'   => 8,
    ],

    //订单商品字段验证
    'ORDER_GOODS_VALIDATE' => [
        'no|序号'                 => 'require',
        'client_code|客户编号'      => 'require',
        'user_order_no|客户订单自编号' => 'require',
        'goods_name|货品名称'       => 'require',
        'bar_code|条形码'          => 'require',
        'specification|规格型号'    => 'require',
        'goods_price|单价'        => 'require',
        'goods_number|数量'       => 'require',
        'goods_total|总价'        => 'require',
    ],

    //预报入库-导入模板-仅客户用-模板与数据库字段对应关系
    'INSTOCK_IMPORT' => [
        'match_index'      => 0,
        'warehouse_code'   => 1,
        'bill_no'          => 2,
        'client_code'      => 3,
        'arrive_time'      => 4,
        'instock_type'     => 5,
        'trade_mode'       => 6,
        'arrive_mode'      => 7,
        'transport_number' => 8,
        'client_concat'    => 9,
        'client_phone'     => 10,
        'note'             => 11,
    ],

    //模本必填, 有效性验证规则, 用于支持验证器
    'INSTOCK_VALIDATE' => [
        'match_index|单据序号'        => 'require|number',
        'warehouse_code|仓库编码'     => 'require',
        'bill_no|单据编号'            => 'max:55',
        'client_code|客户端编号'       => 'require',
        'arrive_time|预计到货时间'      => 'require',
        'instock_type|入库性质'       => 'require|max:1|number',
        'trade_mode|入库类型'         => 'require|max:4|number|in:1210,9610,0110,0000',
        'arrive_mode|预到货方式'       => 'require|max:45',
        'transport_number|运输工具编号' => 'max:55',
        'client_concat|客户联系人'     => 'require|max:60',
        'client_phone|客户联系人电话'    => 'require|max:18',
        'note|备注'                 => 'max:255',
    ],

    'INSTOCK_GOODS_IMPORT' => [
        'match_index'     => 0,
        'goods_name'      => 1,
        'bar_code'        => 2,
        'forecast_number' => 3,
    ],

    'INSTOCK_GOODS_VALIDATE' => [
        'match_index|单据序号'       => 'require|number',
        'goods_name|商品名'         => 'max:255',
        'bar_code|商品条码'          => 'require|max:20',
        'forecast_number|预报入库数量' => 'require|max:10|number',
    ],

    //库位导入模板
    'LOCATION_IMPORT' => [
        'location_no'   => 0,
        'location_type' => 1,
        'area_no'       => 2,
        'length'        => 3,
        'width'         => 4,
        'high'          => 5,
        'location_desc' => 6,
    ],

    'STOCK_IMPORT' => [
        'no'           => 0,
        'client_code'  => 1,
        'short_name'   => 2,
        'sku'          => 3,
        'bar_code'     => 4,
        'goods_name'   => 5,
        'location_no'  => 6,
        'stock_number' => 7,
    ],

    'STOCK_IMPORT_VALIDATE' => [
        'no|序号'             => 'require|integer',
        'client_code|客户编号'  => 'require|max:80',
        'short_name|客户简称'   => 'max:255',
        'sku|货品SKU号'        => 'require|max:30',
        'bar_code|货品条码'     => 'max:20',
        'goods_name|货品名称'   => 'max:255',
        'location_no|库位编号'  => 'require|max:30',
        'stock_number|库位数量' => 'require|integer',
    ],

    //分配运单导入
    'WAYBILL_IMPORT' => [
        'no'            => 0,
        'client_code'   => 1,
        'user_order_no' => 2,
        'shipment_code' => 3,
        'shipping_no'   => 4,
    ],

    'WAYBILL_IMPORT_VALIDATE' => [
        'no|序号'                 => 'require',
        'client_code|客户编号'      => 'require',
        'user_order_no|订单企业自编号' => 'require',
        'shipment_code|物流商编号'   => 'require',
        'shipping_no|运单号'       => 'require',
    ],
];
