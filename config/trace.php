<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

// | Trace设置 开启 app_trace 后 有效
// +----------------------------------------------------------------------
return [
    // 内置Html Console 支持扩展
    'type' => 'Html',
];
