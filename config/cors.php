<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */
return [
    //应用接口访问登录模式状态--- 测试OR开发
    //不适用于app_status的应用场景切换,这里单独为session和cookie机制做一个模式切换状态--DEVELOP(默认)--PRODUCT
    'auth_status' => env('AUTH_STATUS', 'DEVELOP'),

    //测试阶段的SESSION配置AUTH_STATUS为DEVELOP时系统会应用这些SESSION统一将未登录账户识别为测试账号
    'develop_session' => [
        'user'=> [
            'id'    => 2,
            'account'  => 'test',
            'phone' => '13022076770',
            'email' => '123456@126.com',
            'id_card' => '110564199901014511',
            'real_name' => 'chen',
            'login_time' => '1591234560',
            'create_time' => '2020-07-04 15:20:00',
            'status' => 1,
            'is_delete' => 0,
            'authorize' => 1,
            'is_super' => 0,
            'warehouse_id' => 1,
        ],
    ],

    //跨域访问白名单配置
    'auth_white_list' => explode(',', env('CORS_AUTH_WHITE_LIST')),
];
