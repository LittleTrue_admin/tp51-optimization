<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */

// | Cookie设置
// +----------------------------------------------------------------------
return [
    // cookie 名称前缀
    'prefix' => '',
    // cookie 保存时间
    'expire' => 0,
    // cookie 保存路径
    'path' => '/',
    // cookie 有效域名
    'domain' => env('COOKIE_DOMAIN'),
    // cookie 启用安全传输
    'secure' => false,
    // httponly 设置
    'httponly' => '',
    // 是否使用 setcookie
    'setcookie' => true,
];
