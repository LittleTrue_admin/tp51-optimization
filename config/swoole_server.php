<?php
/**
 *  @department : Commercial development.
 *  @description : This file is part of [example].
 *  example all rights reserved.
 */
use think\facade\Env;

// +----------------------------------------------------------------------
// | Swoole设置 php think swoole:server 命令行下有效
// +----------------------------------------------------------------------
return [
    // 扩展自身配置
    'host'         => '0.0.0.0', // 监听地址
    'port'         => 9508, // 监听端口
    'type'         => 'socket', // 服务类型 支持 socket http server
    'mode'         => '', // 运行模式 默认为SWOOLE_PROCESS
    'sock_type'    => '', // sock type 默认为SWOOLE_SOCK_TCP
    'swoole_class' => 'app\http\Swoole', // 自定义服务类名称 --- 定义后其余参数不再有效

    // 可以支持swoole的所有配置参数 -- 以下配置仅作为demo,不再有效
    'daemonize' => false,
    'pid_file'  => Env::get('runtime_path') . 'swoole_server.pid',
    'log_file'  => Env::get('runtime_path') . 'swoole_server.log',

    // 事件回调定义 -- 以下配置仅作为demo,不再有效
    'onOpen' => function ($server, $request) {
        echo "server: handshake success with fd{$request->fd}\n";
    },

    'onMessage' => function ($server, $frame) {
        echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";
        $server->push($frame->fd, 'this is server');
    },

    'onRequest' => function ($request, $response) {
        $response->end('<h1>Hello Swoole. #' . rand(1000, 9999) . '</h1>');
    },

    'onClose' => function ($ser, $fd) {
        echo "client {$fd} closed\n";
    },
];
