FROM registry.cn-shenzhen.aliyuncs.com/example-erp/docker-erp:latest

MAINTAINER gxz 644812368@qq.com

# 更新nginx配置&&新增前后端分离项目配置
ADD ./docker/etc/nginx/nginx.conf /etc/nginx/
ADD ./docker/etc/nginx/sites.d/example.conf /etc/nginx/sites.d/

RUN sed -i "s/;mssql.charset = \"ISO-8859-1\"/mssql.charset = \"utf-8\"/" /etc/php/php.ini
&& sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 300M/" /etc/php/php.ini
&& sed -i "s/max_file_uploads = 20/max_file_uploads = 300/" /etc/php/php.ini
&& sed -i "s/post_max_size = 10M/post_max_size = 300M/" /etc/php/php.ini
&& sed -i "s/max_execution_time = 30/max_execution_time = 120/" /etc/php/php.ini
&& sed -i "s/pm.max_children = 5/pm.max_children = 100/" /usr/local/php/etc/php-fpm.d/www.conf
&& sed -i "s/; max_input_vars = 1000/max_input_vars = 3000/" /etc/php/php.ini

RUN localedef -i zh_CN -f UTF-8 zh_CN.UTF-8
ENV LANG zh_CN.UTF-8

# 配置生效环境变量
RUN source /etc/profile

# 项目目录 -- 前后端 -- 挂载位于宿主机的项目
RUN  mkdir -p  /data/www/html/example

ADD . /data/www/html/example
RUN chown www:www -R /data/www/html/example

# 开启端口
EXPOSE 80

# 配置生效环境变量
RUN source /etc/profile

# 项目工作目录
WORKDIR /data/www/html/example

# 开启php拓展swoole
RUN /usr/local/php/bin/pecl install swoole && echo "extension=swoole.so" >> /etc/php/php.ini

# composer初始化
RUN composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
RUN composer install --no-dev \
    && composer dump-autoload -o \
    && composer clearcache

# 运行supervisord进行监控
CMD ["/usr/bin/supervisord"]